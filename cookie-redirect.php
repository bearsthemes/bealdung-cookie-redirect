<?php
/*
  Plugin Name: Cookie Redirect
  Plugin URI: https://www.baeldung.com/
  Description: This plugins allows you to create cookie and redirect page.
  Author: Tom Pham
  Text Domain: cookie-redirect
  Domain Path: /languages
  Version: 1.0
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
define( 'COOKIE_REDIRECT_URL', plugin_dir_url( __FILE__ ) );

/* Setting Backend */
require_once('admin/class-cookie-redirect-page.php');

/* Front End */
require_once('front-end/cookie-redirect-page.php');
