<?php
//check cookie page and redirect
add_action('template_redirect','check_cookie_redirect_page');
if(!function_exists('check_cookie_redirect_page')):
  function check_cookie_redirect_page(){
    if(is_page() && !is_admin()){
      $cookie_redirect_option = get_option( 'cookie-redirect' );
      $page_id = get_the_ID();
      foreach ($cookie_redirect_option as $key => $value) {
        if($page_id == $value['page-url']){
          if(isset($_COOKIE[$value['cookie-name']]) && $_COOKIE[$value['cookie-name']] != ''){
            if($value['redirect-has-cookie'] != '' && filter_var($value['redirect-has-cookie'], FILTER_VALIDATE_URL) !== FALSE){
              wp_redirect( $value['redirect-has-cookie'] );exit;
            }
          }else{
            if($value['redirect-non-cookie'] != '' && filter_var($value['redirect-non-cookie'], FILTER_VALIDATE_URL) !== FALSE){
              wp_redirect( $value['redirect-non-cookie'] );exit;
            }
          }
        }
      }
    }
  }
endif;

?>
