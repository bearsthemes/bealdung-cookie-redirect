<?php
/**
 * Created by TomPham
 * Date: 11/11/2019
 */

class OptionCookieRedirect{

	function __construct() {
		// Add menu option redirect
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );

		// Add js in admin
		 add_action( 'admin_enqueue_scripts',[ $this, 'cookie_enqueue_scripts' ]  );
	}

	function admin_menu(){
		add_menu_page(
        __( 'Cookie Redirect', 'cookie-redirect' ),
	        'Cookie Redirect',
	        'manage_options',
	        'cookie-redirect',
        [ $this,'cookie_redirect_callback' ],
        'dashicons-external',
        8
    );
	}

	function cookie_enqueue_scripts(){
		if(isset($_GET['page']) && $_GET['page'] == 'cookie-redirect'){
			//css
			wp_enqueue_style( 'select-min', 'https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css');
			wp_enqueue_style( 'admin-style-repeater',  COOKIE_REDIRECT_URL.'assets/css/admin.css');
			//js
			wp_enqueue_script('jquery.min', 'https://code.jquery.com/jquery-1.12.4.min.js', array('jquery'));
			wp_enqueue_script( 'select-min', 'https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js');
			wp_enqueue_script('repeater', COOKIE_REDIRECT_URL.'assets/js/repeater.js', array('jquery'));
			wp_enqueue_script('admin-custom', COOKIE_REDIRECT_URL.'assets/js/admin-custom.js', array('jquery'));
		}
	}

	function cookie_redirect_callback(){
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		$pages = get_pages();
		$cookie_redirect_name = 'cookie-redirect';
		// Read in existing option value from database
		$cookie_redirect_option = get_option( $cookie_redirect_name );
		if( isset($_POST['Submit'])){
				$cookie_redirect_option = $_POST[ $cookie_redirect_name ];
				update_option( $cookie_redirect_name, $cookie_redirect_option );
			?><div class="updated"><p><strong><?php echo __('Settings saved.'); ?></strong></p></div><?php
		}
		?>
		<div class="wrap">
			<h1><?php echo __('Cookie Redirect','cookie-redirect') ?></h1>
			<form name="option-cookie-redirect" method="post" action="">
				<div id="cookie-page-repeater">
					<?php if($cookie_redirect_option): ?>
					<?php foreach ($cookie_redirect_option as $key => $value): ?>
						<div class="items" data-group="cookie-redirect">
							<!-- Repeater Content -->
							<div class="item-content">
								<p><b><?php _e("Page Redirect URL:", 'cookie-redirect' ); ?> <span class="required">*</span></b>
									<select class="item-field"  data-name="page-url" required>
										<option value="">--Select Page--</option>
										<?php
										foreach ( $pages as $page ) {
												if($page->post_title == '') continue;
												$selected = ($value['page-url'] == $page->ID)?'selected="selected"':'';
												$option = '<option value="' . $page->ID . '" '.$selected.'>';
												$option .= $page->post_title;
												$option .= '</option>';
												echo $option;
											}
										 ?>
									</select>
								</p>
								<p><b><?php _e("Cookie Name:", 'cookie-redirect' ); ?> <span class="required">*</span></b>
									<input type="text" data-name="cookie-name" data-value="<?php echo $value['cookie-name'] ?>" placeholder="Cookie Name" required >
								</p>
								<p><b><?php _e("Redirect has cookie URL:", 'cookie-redirect' ); ?></b>
									<input type="url" data-name="redirect-has-cookie" data-value="<?php echo $value['redirect-has-cookie'] ?>"placeholder="http:// or https://">
								</p>
								<p><b><?php _e("Redirect non-cookie URL:", 'cookie-redirect' ); ?></b>
									<input type="url" data-name="redirect-non-cookie" data-value="<?php echo $value['redirect-non-cookie'] ?>"  placeholder="http:// or https://">
								</p>
							</div>

							<!-- Repeater Remove Btn -->
							<div class="pull-right repeater-remove-btn">
									<a href="JavaScript:;" id="remove-btn" class="btn btn-danger" onclick="$(this).parents('.items').remove()">
											<span class="dashicons dashicons-no-alt"></span>
									</a>
							</div>
						</div>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="items" data-group="cookie-redirect">
					  <!-- Repeater Content -->
					  <div class="item-content">
							<p><b><?php _e("Page Redirect URL:", 'cookie-redirect' ); ?> <span class="required">*</span></b>
								<select class="item-field"  data-name="page-url" required>
									<option value="">--Select Page--</option>
									<?php
									foreach ( $pages as $page ) {
											if($page->post_title == '') continue;
									    $option = '<option value="' . $page->ID . '">';
									    $option .= $page->post_title;
									    $option .= '</option>';
									    echo $option;
									  }
									 ?>
								</select>
							</p>
							<p><b><?php _e("Cookie Name:", 'cookie-redirect' ); ?> <span class="required">*</span></b>
								<input type="text" data-name="cookie-name" placeholder="Cookie Name" required>
							</p>
							<p><b><?php _e("Redirect has cookie URL:", 'cookie-redirect' ); ?></b>
								<input type="url" data-name="redirect-has-cookie" placeholder="http:// or https://">
							</p>
							<p><b><?php _e("Redirect non-cookie URL:", 'cookie-redirect' ); ?></b>
								<input type="url" data-name="redirect-non-cookie" placeholder="http:// or https://">
							</p>
					  </div>

					  <!-- Repeater Remove Btn -->
					  <div class="pull-right repeater-remove-btn">
					      <a href="JavaScript:;" id="remove-btn" class="btn btn-danger" onclick="$(this).parents('.items').remove()" title="Remove Item">
					          <span class="dashicons dashicons-no-alt"></span>
					      </a>
					  </div>
					</div>
				<?php endif; ?>
				</div>
				<!-- Repeater Heading -->
				<div class="repeater-heading">
						<a href="JavaScript:;" class="btn btn-primary repeater-add-btn">
								<?php echo __('Add new redirect','cookie-redirect'); ?>
						</a>
				</div>
				<p class="submit">
					<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
				</p>
			</form>
		</div>
		<?php
	}

}

/* only run in admin */
if ( is_admin() ) {
	$option_sorting_priority = new OptionCookieRedirect();
}
