=== Cookie Redirect ===

This plugins allows you to create cookie and redirect page.

Ordered list:

1. Add redirect type
2. Redirect follow cookie

== Installation ==
1. Upload "coolie-redirect" to the "/wp-content/plugins/" directory.
2. Activate the plugin through the "Plugins" menu in WordPress.
3. Go menu "Categories" in Posts

= 1.0 =
* Initial release.
